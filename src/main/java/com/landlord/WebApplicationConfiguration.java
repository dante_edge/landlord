package com.landlord;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class WebApplicationConfiguration {
	public static void main(String[] args) {
		SpringApplication.run(WebApplicationConfiguration.class, args);
	}
}
