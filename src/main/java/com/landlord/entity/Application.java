package com.landlord.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity
@JsonInclude(Include.NON_NULL)
public class Application {
	@Id
	@GeneratedValue
	private long id;
	private long landlordId;
	private Date submitDate;
	private String fullName;
	private Job currentEmployee;
	@Enumerated(EnumType.STRING)
	private Gender gender;
	private Date dayOfBirth;
	private String socialSecurityNumber;
	private String email;
	private String phoneNumber;
	private String address;
	private BigDecimal annualyIncome;
	private BigDecimal bankDeposit;
	@Lob
	private String previousAddress;
	@Lob
	private String previousEmployee;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Date getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public Job getCurrentEmployee() {
		return currentEmployee;
	}
	public void setCurrentEmployee(Job currentEmployee) {
		this.currentEmployee = currentEmployee;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public Date getDayOfBirth() {
		return dayOfBirth;
	}
	public void setDayOfBirth(Date dayOfBirth) {
		this.dayOfBirth = dayOfBirth;
	}
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getAnnualyIncome() {
		return annualyIncome;
	}
	public void setAnnualyIncome(BigDecimal annualyIncome) {
		this.annualyIncome = annualyIncome;
	}
	public BigDecimal getBankDeposit() {
		return bankDeposit;
	}
	public void setBankDeposit(BigDecimal bankDeposit) {
		this.bankDeposit = bankDeposit;
	}
	public String getPreviousAddress() {
		return previousAddress;
	}
	public void setPreviousAddress(String previousAddress) {
		this.previousAddress = previousAddress;
	}
	public String getPreviousEmployee() {
		return previousEmployee;
	}
	public void setPreviousEmployee(String previousEmployee) {
		this.previousEmployee = previousEmployee;
	}
	public long getLandlordId() {
		return landlordId;
	}
	public void setLandlordId(long landlordId) {
		this.landlordId = landlordId;
	}
	
}