package com.landlord.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Job {
	@Column(name = "current_Employee_title")
	private String title;
	@Column(name = "current_Employee_organization")
	private String organization;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
}
