package com.landlord.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.landlord.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Long>{
	public Account findByLoginName(String loginName);
}
