package com.landlord.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.landlord.entity.Application;

public interface ApplicationRepository extends JpaRepository<Application, Long>{
	List<Application> findByLandlordIdOrderByIdDesc(long landlordId);
}
