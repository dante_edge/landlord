package com.landlord.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;

@Component
@Order(20)
public class AccessControlFilter implements Filter {
	private final List<WhiteRecord> WHITE_LIST = new ArrayList<>(
			Arrays.asList(new WhiteRecord("post", "/account/default"),
					new WhiteRecord("get", "/mobile/**"),
					new WhiteRecord("get", "/index.html"),
					new WhiteRecord("get", "/landlord/*"),
					new WhiteRecord("post", "/application"),
					new WhiteRecord("post", "/account/default/token")));
	String domain = "landlord.com";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		boolean shouldBypass = false;
		if (httpServletRequest.getUserPrincipal() == null) {
			shouldBypass = isInWhiteList(httpServletRequest.getMethod(), getRequestedURI(httpServletRequest));
		} else {
			shouldBypass = true;
		}

		if (shouldBypass) {
			chain.doFilter(request, response);
		} else {
			setUnauthorizedResponse(httpServletResponse);
		}
	}

	private boolean isInWhiteList(String method, String uri) {

		for (WhiteRecord record : WHITE_LIST) {
			boolean result = record.match(method, uri);
			if (result) {
				return true;
			}
		}

		return false;
	}

	private void setUnauthorizedResponse(HttpServletResponse httpServletResponse) {
		httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		httpServletResponse.addHeader("WWW-Authenticate",
				String.format("OAuth realm=\"%s\", oauth_error=\"unauthorized\"", domain));
	}

	static String getRequestedURI(HttpServletRequest httpServletRequest) {
		StringBuilder buf = new StringBuilder();
		if (httpServletRequest.getServletPath() != null) {
			buf.append(httpServletRequest.getServletPath());
			if (httpServletRequest.getPathInfo() != null) {
				buf.append(httpServletRequest.getPathInfo());
			}
		} else {
			buf.append(httpServletRequest.getRequestURI().substring(httpServletRequest.getContextPath().length()));
		}

		String uri = buf.toString();
		return uri;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

	private static class WhiteRecord {
		final String method;
		final String uri;

		private static final AntPathMatcher pathMatcher = new AntPathMatcher();
		private static final AntPathMatcher methodMatcher = new AntPathMatcher();

		WhiteRecord(String method, String uri) {
			super();
			this.method = method;
			this.uri = uri;
			methodMatcher.setCaseSensitive(false);
		}

		boolean match(String method, String uri) {
			return methodMatcher.match(this.method.toLowerCase(), method) && pathMatcher.match(this.uri, uri);
		}
	}
	
	public void addWhiteList(String methodPattern, String uriPattern){
		WHITE_LIST.add(new WhiteRecord(methodPattern, uriPattern));
	}
}
