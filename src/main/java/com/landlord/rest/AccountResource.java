package com.landlord.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.landlord.entity.Account;
import com.landlord.service.AccessToken;
import com.landlord.service.AccountService;
import com.landlord.service.impl.AccountAlreadyTakenException;

@RestController
@RequestMapping("/account")
public class AccountResource {
	
	@Autowired
	AccountService accountService;
	
	@RequestMapping(value = "/default/token", method = RequestMethod.POST, produces = "application/json")
	public AccessTokenResponse signIn(
			@RequestParam(value = "username", required = true) String loginName, 
			@RequestParam(value = "password", required = true) String password, 
			@RequestParam(value = "grant_type", required = true) String grant_type) throws InvalidGrantException {
		AccessToken token = accountService.signIn(loginName, password);
		if (token == null){
			throw new InvalidGrantException();
		}
		else {
			return new AccessTokenResponse(token.getToken(), "Bearer");
		}
	}
	
	@RequestMapping(value = "/default", method = RequestMethod.POST, produces = "application/json")
	public AccessTokenResponse signUp(
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "username", required = true) String loginName, 
			@RequestParam(value = "password", required = true) String password) throws AccountAlreadyTakenException {
		Account account = new Account();
		account.setName(name);
		account.setLoginName(loginName);
		AccessToken token = accountService.signUp(account, password);
		
		return new AccessTokenResponse(token.getToken(), "Bearer");
	}
	
	private static class AccessTokenResponse {
		@JsonProperty("access_token")
		private final String accessToken;
		@JsonProperty("token_type")
		private final String tokenType;
		@SuppressWarnings("unused")
		public String getAccessToken() {
			return accessToken;
		}
		@SuppressWarnings("unused")
		public String getTokenType() {
			return tokenType;
		}
		
		private AccessTokenResponse(String accessToken, String tokenType) {
			super();
			this.accessToken = accessToken;
			this.tokenType = tokenType;
		}
	}

}
