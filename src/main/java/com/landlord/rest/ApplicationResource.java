package com.landlord.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.landlord.entity.Application;
import com.landlord.service.ApplicationService;
import com.landlord.service.NotFoundException;

@RestController
@RequestMapping("/application")
public class ApplicationResource {
	
	@Autowired
	ApplicationService applicationService;
	
	@RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
	public SimpleResult<List<Application>> findAllApplication(UserPrincipal userPrincipal){
		return new SimpleResult<List<Application>>(applicationService.findByLandlordId(userPrincipal.getAccount().getId()));
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Application findById(@PathVariable("id") long id) throws NotFoundException {
		Application application = applicationService.findById(id);
		if (application == null) {
			throw new NotFoundException();
		}
		else {
			return application;
		}
	}
	
	@RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public void submitApplication(@RequestBody Application application) throws NotFoundException {
		applicationService.save(application);
	}
}
