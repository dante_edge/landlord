package com.landlord.rest;

import java.io.IOException;
import java.security.Principal;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;
import com.landlord.entity.Account;
import com.landlord.service.AccountService;
import com.landlord.service.InvalidAccessTokenException;

@Component
@Order(0)
public class AuthenticationFilter implements Filter{
	private final Logger logger = LoggerFactory
			.getLogger(AuthenticationFilter.class);
	
	@Autowired
	AccountService accountService;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {		
	}

	@Override
	public void destroy() {		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;
		String accessToken = getAccessToken(request);
		logger.debug("Request with authentication: {}", accessToken);
		HttpServletRequest forwardRequest = null;
		if (!Strings.isNullOrEmpty(accessToken)) {
			Account account = verifyAccessToken(accessToken);
			if (account == null) {
				sendInvalidAccessTokenResponse(request, response);
				return;
			}
			else {
				forwardRequest = new RequestWarpper(request, new UserPrincipal(account, request.getRemoteAddr()));
			}
		}
		
		forwardRequest = forwardRequest == null ? request : forwardRequest;
		
		filterChain.doFilter(forwardRequest, response);
	}


	private void sendInvalidAccessTokenResponse(HttpServletRequest request, HttpServletResponse response) {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
		response.addHeader(
				"WWW-Authenticate",
				"OAuth realm=\"One Zero\", oauth_error=\"Invalid Access Token\"");
	}

	private Account verifyAccessToken(String accessToken) {
		int firstSpaceIndex = accessToken.indexOf(" ");
		
		if (firstSpaceIndex == -1) {
			return null;
		}
		
		String authMethod = accessToken.substring(0, firstSpaceIndex).trim();
		String token = accessToken.substring(firstSpaceIndex).trim();
		
		if(!authMethod.equalsIgnoreCase("Bearer") || Strings.isNullOrEmpty(token)) {
			return null;
		}
		
		try{
			return accountService.verifyAccessToken(token);
		}
		catch(InvalidAccessTokenException e) {
			return null;
		}
	}

	private String getAccessToken(HttpServletRequest request) {
		String headerValue = request.getHeader("Authorization");
		if (!Strings.isNullOrEmpty(headerValue)) {
			return headerValue;
		}
		else {
			return null;
		}		
	}
	
	private static class RequestWarpper extends HttpServletRequestWrapper {

		private final Principal principal;

		public RequestWarpper(HttpServletRequest request, Principal principal) {
			super(request);
			this.principal = principal;
		}

		@Override
		public Principal getUserPrincipal() {
			return this.principal;
		}

	}
}
