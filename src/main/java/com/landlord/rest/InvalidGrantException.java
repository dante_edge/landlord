package com.landlord.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidGrantException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2868850509247412870L;

}
