package com.landlord.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.landlord.entity.Account;
import com.landlord.service.AccountService;
import com.landlord.service.NotFoundException;

@RestController
@RequestMapping("/landlord")
public class LandloadProfileResource {
	
	@Autowired
	AccountService accountService;
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public Landlord findLandlordById(@PathVariable("id") long id) throws NotFoundException{
		Account account = accountService.findById(id);
		if (account == null) {
			throw new NotFoundException();
		}
		else {
			return new Landlord(account);
		}
	}
	
	public static class Landlord{
		private String name;
		private long id;
		public Landlord(Account account) {
			this.id = account.getId();
			this.name = account.getName();
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		
	}
}
