package com.landlord.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.landlord.service.ApplicationService;

@RestController
@RequestMapping("/send")
public class SendApplicationResource {
	
	@Autowired(required = true)
	ApplicationService applicationService;
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void send(@RequestParam("content")final String content, @RequestParam("recipient")final String recipient, final UserPrincipal userPrincipal) {
		applicationService.sendEmail(content, recipient, userPrincipal.getAccount());
	}
}