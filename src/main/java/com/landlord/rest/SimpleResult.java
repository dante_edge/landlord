package com.landlord.rest;

public class SimpleResult<T> {
	private final T result;

	public SimpleResult(T result) {
		super();
		this.result = result;
	}

	public T getResult() {
		return result;
	}
	
	
}
