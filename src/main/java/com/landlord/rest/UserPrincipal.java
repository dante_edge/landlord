package com.landlord.rest;

import java.security.Principal;

import com.landlord.entity.Account;

public class UserPrincipal implements Principal {

	private Account account;
	private String remoteAddr;

	public UserPrincipal(Account account, String remoteAddr) {
		super();
		this.account = account;
		this.remoteAddr = remoteAddr;
	}
	
	public Account getAccount() {
		return account;
	}

	public String getRemoteAddr() {
		return remoteAddr;
	}

	@Override
	public String getName() {
		return account.getName();
	}


	public void setAccount(Account account) {
		this.account = account;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}
}
