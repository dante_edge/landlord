package com.landlord.service;

import com.landlord.entity.Account;

public interface AccessToken {
	public Account getAccount();
	public String getToken();
}