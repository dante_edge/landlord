package com.landlord.service;

import com.landlord.entity.Account;
import com.landlord.service.impl.AccountAlreadyTakenException;

public interface AccountService {
	AccessToken signUp(Account newAccount, String password) throws AccountAlreadyTakenException;
	AccessToken signIn(String loginName, String password);
	Account verifyAccessToken(String token);
	Account findById(long id);
}
