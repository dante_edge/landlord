package com.landlord.service;

import java.util.List;

import com.landlord.entity.Account;
import com.landlord.entity.Application;

public interface ApplicationService {

	List<Application> findByLandlordId(long landlordId);

	Application findById(long id);
	
	void sendEmail(final String content, final String recipient, final Account account);

	void save(Application application) throws NotFoundException;
}
