package com.landlord.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.UNAUTHORIZED, reason="Invalid Access Token")
public class InvalidAccessTokenException extends RuntimeException {
	private static final long serialVersionUID = -5901310325045907551L;
	
}