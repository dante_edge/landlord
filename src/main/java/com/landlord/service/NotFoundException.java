package com.landlord.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4603654433878063425L;

}
