package com.landlord.service.impl;

import com.landlord.entity.Account;
import com.landlord.service.AccessToken;

class AccessTokenImpl implements AccessToken {
	final Account account;
	final String token;
	
	public AccessTokenImpl(Account account, String token) {
		super();
		this.account = account;
		this.token = token;
	}
	public Account getAccount() {
		return account;
	}
	public String getToken() {
		return token;
	}
	
	
}