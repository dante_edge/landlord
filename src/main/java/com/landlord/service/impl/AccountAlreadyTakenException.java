package com.landlord.service.impl;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class AccountAlreadyTakenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3454355469578275713L;

}
