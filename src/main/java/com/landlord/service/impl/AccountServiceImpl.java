package com.landlord.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.landlord.entity.Account;
import com.landlord.repository.AccountRepository;
import com.landlord.service.AccessToken;
import com.landlord.service.AccountService;
import com.landlord.service.InvalidAccessTokenException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class AccountServiceImpl implements AccountService {

	private final SecretKey key = new SecretKeySpec(new byte[] { (byte) 0x62, (byte) 0x71, (byte) 0xa2, (byte) 0xad,
			(byte) 0xb3, (byte) 0x77, (byte) 0xc5, (byte) 0x04, (byte) 0x8b, (byte) 0xba, (byte) 0x0a, (byte) 0x7a,
			(byte) 0xc1, (byte) 0xce, (byte) 0xca, (byte) 0x78, (byte) 0x21, (byte) 0x50, (byte) 0x46, (byte) 0x36,
			(byte) 0x2a, (byte) 0xa7, (byte) 0x2b, (byte) 0x82, (byte) 0x2a, (byte) 0xfb, (byte) 0xc3, (byte) 0x4a,
			(byte) 0xf2, (byte) 0x5a, (byte) 0xa5, (byte) 0x0c }, "HMacSHA256");
		
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED, isolation=Isolation.READ_COMMITTED)
	public AccessToken signUp(Account newAccount, String password) throws AccountAlreadyTakenException {
		
		if (accountRepository.findByLoginName(newAccount.getLoginName()) != null) {
			throw new AccountAlreadyTakenException();
		}
		
		setPassword(newAccount, password);
		newAccount.setCreatedAt(new Date());
		Account result = accountRepository.save(newAccount);
		return issueAccessToken(result);
	}
	
	@Override
	public AccessToken signIn(String loginName, String password) {
		Account account = accountRepository.findByLoginName(loginName);
		if (account != null && verifyPassword(account, password)) {
			return issueAccessToken(account);
		}
		else {
			return null;
		}
	}
	
	@Override
	public Account verifyAccessToken(String token) {
		try {
			Jws<Claims> jws = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
			return accountRepository.findOne(Long.parseLong(jws.getBody().getSubject()));
		} catch (Exception ex) {
			throw new InvalidAccessTokenException();
		}
	}
	
	private AccessToken issueAccessToken(Account account) {
		String token = Jwts.builder().setSubject(String.valueOf(account.getId()))
				.signWith(SignatureAlgorithm.HS256, key).compact();

		return new AccessTokenImpl(account, token);
	}
	
	
	private boolean verifyPassword(Account account, String password) {
		if (account == null || StringUtils.isBlank(account.getPasswordHash())) {
			return false;
		}
		else {
			return account.getPasswordHash().equalsIgnoreCase(caculatePasswordHash(account, password));
		}
	}
	
	private void setPassword(Account account, String password) {
		account.setPasswordHash(caculatePasswordHash(account, password));
	}
	
	private String caculatePasswordHash(Account account, String password) {
		try{
			return Hex.encodeHexString(DigestUtils.getSha384Digest().digest(String.format("%s:%s", account.getLoginName(), password).getBytes("UTF-8")));
		}
		catch(UnsupportedEncodingException e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public Account findById(long id) {
		return accountRepository.findOne(id);
	}
}
