package com.landlord.service.impl;

import java.util.Date;
import java.util.List;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.landlord.entity.Account;
import com.landlord.entity.Application;
import com.landlord.repository.ApplicationRepository;
import com.landlord.service.AccountService;
import com.landlord.service.ApplicationService;
import com.landlord.service.NotFoundException;

@Service
public class ApplicationServiceImpl implements ApplicationService{
	@Autowired
	ApplicationRepository applicationRepository;
	@Autowired
	AccountService accountService;
	
	@Autowired(required = true)
	JavaMailSender javaMailSender;
	
	@Override
	public List<Application> findByLandlordId(long landlordId){
		return applicationRepository.findByLandlordIdOrderByIdDesc(landlordId);
	}

	@Override
	public Application findById(long id) {
		return applicationRepository.findOne(id);
	}

	@Override
	@Async
	public void sendEmail(final String content, final String recipient, final Account account) {
		javaMailSender.send(new MimeMessagePreparator(){

			@Override
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
				helper.setTo(recipient);
				helper.setFrom(account.getLoginName());
				helper.setSubject("Application form from " + account.getName());
				helper.setText(content);
			}
			
		});
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(Application application) throws NotFoundException {
		Account landlord = accountService.findById(application.getLandlordId());
		if (landlord == null){
			throw new NotFoundException();
		}
		application.setId(0);
		application.setSubmitDate(new Date());
		applicationRepository.save(application);
	}
}
