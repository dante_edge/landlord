CREATE TABLE account (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	login_name VARCHAR(128)  NULL,
 	name VARCHAR(128) NULL,
 	password_hash VARCHAR(120) NOT NULL,
 	created_at TIMESTAMP NOT NULL DEFAULT NOW(),
	UNIQUE KEY `idx_account_username` (`login_name`)
);

CREATE TABLE application (
	id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	landlord_id BIGINT NOT NULL,
	submit_date TIMESTAMP NOT NULL,
	full_name VARCHAR(255) NOT NULL,
	current_Employee_title VARCHAR(255) NOT NULL,
	current_Employee_organization VARCHAR(255) NOT NULL,
	gender VARCHAR(10) NOT NULL,
	day_of_Birth TIMESTAMP NOT NULL,
	social_security_number VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	phone_number VARCHAR(255) NOT NULL,
	address VARCHAR(255) NOT NULL,
	annualy_income NUMERIC(15,2) NULL DEFAULT 0,
	bank_deposit NUMERIC(15,2) NULL DEFAULT 0,
	previous_address LONGTEXT NOT NULL,
	previous_employee LONGTEXT NOT NULL
);

CREATE INDEX idx_application_to_landlord on application(landlord_id);