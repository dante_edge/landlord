package com.landlord.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.landlord.rest.AccessControlFilter;

@Component
public class H2ConsoleConfiguration {
	@Autowired
	public void setAccessControllFilter(AccessControlFilter filter) {
		filter.addWhiteList("*", "/h2-console");
		filter.addWhiteList("*", "/h2-console/**");
	}
}
