var ApplicationCell = React.createClass({
	handleTap: function() {
		console.log("Tap!");
		var detail = window.open("application_detail.html#" + this.props.application.id);
		detail.detail = this.props.application;
	},
	render: function() {
		return (
			<li onClick={this.handleTap}>
				<div className="datetime">5 hours ago</div>
				<div className="header"><h2>{this.props.application.fullName}</h2>, {this.props.application.gender}, {this.props.application.submitAt}</div>
				<div className="body">{this.props.application.currentEmployee.title} at {this.props.application.currentEmployee.organization}</div>
			</li>
		);
	}
});

var ApplicationList = React.createClass({
	render: function() {
		var nodes = this.props.data.map(function(application){
			return (
				<ApplicationCell application={application} key={application.id}/>
			);
		});
		return (
			<ul>
				{nodes}
			</ul>
		);
	}
});

var App = React.createClass({
	getInitialState: function(){
		return {data: []};
	},
	componentDidMount: function() {
		$.ajax({
			url: "/application/",
			headers: {
				Authorization: "Bearer " + localStorage.getItem("access_token")
			},
			type: "GET"
		})
		.done(function(data){
			this.setState({data: data.result});
		}.bind(this))
		.fail(function(jqXHR, textStatus, errorThrown){
			if (jqXHR.status == 401) {
				location.href = "sign_in.html";
			}
			else {
				alert(textStatus + " " + errorThrown);
			}
		}.bind(this));
	},
	render: function() {
		return (<ApplicationList data={this.state.data} />);
	}
});


ReactDOM.render(
	<App />,
	document.getElementById("container")
);