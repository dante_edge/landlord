var FormattedDate = ReactIntl.FormattedDate;
var IntlProvider = ReactIntl.IntlProvider;
var FormattedNumber = ReactIntl.FormattedNumber;

var Detail = React.createClass({
	getInitialState: function() {
		return {detail: false};
	},
	componentDidMount: function() {
		if (location.hash) {
			var id = location.hash.substring(1);
			$.ajax({
				url: "/application/" + encodeURIComponent(id),
				type: "GET",
				headers: {
					Authorization: "Bearer " + localStorage.getItem("access_token") 
 				}
			})
			.done(function(detail){
				this.setState({detail: detail});
			}.bind(this))
			.fail(function(jqXHR){
				if(jqXHR.status = 401) {
					location.href = "sign_in.html";
				}
			}.bind(this))
		}
	},
	render: function() {
		var c;
		if (this.state.detail){
		c =  (
				<div id="body">
				<div id="header">
					<img src="images/send.png" width="22" height="22" />
					<h1>{this.state.detail.fullName}</h1>
				</div>

				<div id="container">
					<section>
						<h2>Basic</h2>
						<p>Gender: {this.state.detail.gender}</p>
						<p>Day of Birth: <FormattedDate value={this.state.detail.dayOfBirth}/></p>
						<p>Social Security: {this.state.detail.socialSecurityNumber}</p>
					</section>

					<section>
						<h2>Contact</h2>
						<p>Email: {this.state.detail.email}</p>
						<p>Phone: {this.state.detail.phoneNumber}</p>
					</section>

					<section>
						<h2>Address</h2>
						<p>{this.state.detail.address}</p>
					</section>

					<section>
						<h2>Current Employee</h2>
						<p>{this.state.detail.currentEmployee.title} at {this.state.detail.currentEmployee.organization}</p>
					</section>

					<section>
						<p>Annualy Income: <FormattedNumber value={this.state.detail.annualyIncome} style="currency" currency="USD"/></p>
						<p>Bank Deposit: <FormattedNumber value={this.state.detail.bankDeposit} style="currency" currency="USD"/></p>
					</section>

					<section>
						<h2>Previous Address within 5 Years</h2>
						<p>{this.state.detail.previousAddress}</p>
					</section>

					<section>
						<h2>Previous Employee within 2 Years</h2>
						<p>{this.state.detail.previousEmployee}</p>
					</section>
				</div>
			</div>);
		}
		else {
			c = (<p>Loading...</p>);
		}
		return c;
	}
});

ReactDOM.render(
	<IntlProvider locale={navigator.language}>
		<Detail />
	</IntlProvider>
	,
	document.getElementById("stage")
);