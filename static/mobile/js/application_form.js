var Header = React.createClass({
	getInitialState: function(){
		return {landlord: {name: "", id: 0}};
	},
	componentDidMount: function(){
		var landloardId = location.hash.substring(1);
    	$.ajax({
    		url: "/landlord/" + encodeURIComponent(landloardId),
    		type: "GET"
    	})
    	.done(function(landlord){
    		this.setState({landlord: landlord})
    	}.bind(this))
    	.fail(function(jqXHR){
    		//TODO: inform use to try again
    	}.bind(this));
	},
	handleSend: function() {
		$("input[type=submit]").click();
	},
	render: function() {
		return (
			<div id="header">
				<img src="images/send.png" width="22" height="22" onClick={this.handleSend}/>
				<h1>Submit Application to {this.state.landlord.name}</h1>
			</div>
		);
	}
});

var ApplicationForm = React.createClass({
	getInitialState: function(){
		return {
				"submitDate": "",
				"fullName": "",
				"currentEmployeeTitle":"",
				"currentEmployeeOrganization": "",
				"gender":"Male",
				"dayOfBirth":"",
				"socialSecurityNumber":"",
				"email":"",
				"phoneNumber":"",
				"address":"",
				"annualyIncome": 0.00,
				"bankDeposit": 0.00,
				"previousAddress":"",
				"previousEmployee":""
		};
	},
	handleFieldChange: function(e) {
		var field = {};
		field[e.target.id] = e.target.value;
		this.setState(field);
	},
	handleSubmit: function(e){
		e.preventDefault();
		this.props.submitHandler(this.state);
	},
	render: function(){
		return (
			<form onSubmit={this.handleSubmit}>

			<div id="container">
				<section id="nameSection">
					<input id="fullName" required="true" onChange={this.handleFieldChange} placeholder="Your name" />
				</section>
				<section>
					<div>
						<label for="gender">Gender</label>
						<select id="gender" onChange={this.handleFieldChange} dir="rtl">
							<option>Male</option>
							<option>Female</option>
						</select>
					</div>
					<div>
						<label for="dayOfBirth">Day of Birth</label>
						<input type="date" id="dayOfBirth" required="true" onChange={this.handleFieldChange} dir="rtl" placeholder="5/4/1995"/>
					</div>
					<div>
						<label for="socialSecurityNumber">Social Security Number</label>
						<input type="text" required="true" onChange={this.handleFieldChange} id="socialSecurityNumber" placeholder="123-456-7890"/>
					</div>
				</section>

				<section>
					<div><label for="email">Email</label><input id="email" onChange={this.handleFieldChange} required="true" type="email" placeholder="shorn@example.org" /></div>
					<div><label for="phoneNumber">Phone</label><input id="phoneNumber" onChange={this.handleFieldChange} required="true" type="text" placeholder="123-(456)789-1234" /></div>
				</section>

				<section><textarea id="address" required="true" onChange={this.handleFieldChange} placeholder="Current Address..." rows="7"></textarea></section>

				<section>
					<div><label for="currentEmployeeTitle">Job Title</label><input required="true" id="currentEmployeeTitle" onChange={this.handleFieldChange} type="text" placeholder="Current position" /></div>
					<div><label for="currentEmployeeOrganization">Organization</label><input required="true" id="currentEmployeeOrganization" onChange={this.handleFieldChange} type="text" placeholder="Organization" /></div>
				</section>

				<section>
					<div><label for="annualyIncome">Income(Annualy)</label><input required="true" id="annualyIncome" onChange={this.handleFieldChange} type="text" placeholder="Your current annualy income" /></div>
					<div><label for="bankDeposit">Bank Deposit</label><input required="true" id="bankDeposit" onChange={this.handleFieldChange} type="text" placeholder="12345.67" /></div>
				</section>

				<section><textarea id="previousAddress" placeholder="Previous Address within 5 Years…" required="true" onChange={this.handleFieldChange} rows="7"></textarea></section>
				<section><textarea id="previousEmployee" placeholder="Previous Employee within 2 Years…" required="true" onChange={this.handleFieldChange} rows="7"></textarea></section>

				<input type="submit" value="Send" />
			</div>
			</form>
		);
	}
});



var App = React.createClass({
	getInitialState: function() {
		return {finished: false};
	},
	handleSubmit: function(application){
		var data = JSON.parse(JSON.stringify(application));
		var landlordId = location.hash.substring(1);
		data.currentEmployee = {
			title: application.currentEmployeeTitle,
			organization: application.currentEmployeeOrganization
		};
		delete data.currentEmployeeTitle;
		delete data.currentEmployeeOrganization;
		data.landlordId = landlordId;
		$.ajax({
			url: "/application",
			data: JSON.stringify(data),
			type: "POST",
			contentType: "application/json"
		})
		.done(function(){
			this.setState({finished: true});
		}.bind(this))
		.fail(function(jqXHR){
			console.log("Failed with status:" + jqXHR.status);
		}.bind(this));
	},
	render: function() {
		var content;
		if (this.state.finished) {
			content = (<p>Well done!<br/>
Your application has been sent.</p>);
		}
		else {
			content = (<ApplicationForm submitHandler={this.handleSubmit}/>);
		}
		return (
			<div>
				<Header />
				{content}
			</div>
		);
	}
})

ReactDOM.render(
	<App />,
	document.getElementById("stage")
);